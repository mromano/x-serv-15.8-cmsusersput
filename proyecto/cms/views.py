from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from django.shortcuts import get_object_or_404
from .models import Contenido, Comentario

from django.contrib.auth import logout
from django.shortcuts import redirect

from django.template import loader

from django.utils import timezone

# Create your views here.

def index(request):
    content_list = Contenido.objects.all()
    context = {'content_list': content_list}
    return render(request, 'cms/homepage.html', context)


@csrf_exempt
def get_content(request, llave):
    if request.user.is_authenticated:
        if request.method == "PUT":
            valor = request.body.decode('utf-8')
        elif request.method == "POST":
            action = request.POST['action']
            if action == "Enviar Contenido":
                valor = request.POST['valor']
        if request.method == "PUT" or (request.method == "POST" and action == "Enviar Contenido"):
            try:
                c = Contenido.objects.get(clave=llave)
                c.valor = valor
            except Contenido.DoesNotExist:
                c = Contenido(clave=llave, valor=valor)
            c.save()
        if request.method == "POST" and action == "Enviar Comentario":
                c = Contenido.objects.get(clave=llave)
                titulo = request.POST['titulo']
                cuerpo = request.POST['cuerpo']
                q = Comentario(contenido=c, titulo=titulo, cuerpo=cuerpo, fecha=timezone.now())
                q.save()
        #formulario para usuarios autenticados
        contenido = get_object_or_404(Contenido, clave=llave)
        context = {'contenido': contenido}
        return render(request, 'cms/contenido_autenticado.html', context)
    else:
        #formulario para usuarios no autenticados
        contenido = get_object_or_404(Contenido, clave=llave)
        context = {'contenido': contenido}
        return render(request, 'cms/contenido_no_autenticado.html', context)

def loggedIn(request):
    if request.user.is_authenticated:
        logged = "Logged in as " + request.user.username + "<a href='/cms/logout'>Logout</a>"
    else:
        logged = "Not logged in. <a href='/admin/'>Login via admin</a>"
    return HttpResponse(logged)

def logout_view(request):
    logout(request)
    return redirect("/cms/")


def imagen(request):
    template = loader.get_template('cms/plantilla.html')
    context = {}
    return HttpResponse(template.render(context, request))
